Kubernetes cluster
==================
Создает кластер Kubernetes. Одновременно производится настройка первой master ноды. Чтобы добавить остальные ноды, воспользуйтесь ролью *K8s nodes*.  
После инициализации кластера в каталоге *files* создаются файлы с командами для подключения master и worker нод, а так же файл *admin.conf*.  

Импорт роли
------------
```yaml
- name: k8s_cluster  
  src: https://gitlab.com/ansible_roles_v/k8s-cluster/  
  version: main  
```

Пример использования
--------------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - k8s_cluster
```